import React from "react";
import { connect, styled } from "frontity";
import Logo_Img from '../../assets/images/blog-bg-1.jpg';
import Location_Icon from '../../assets/images/pin.png';
import Calendar_Icon from '../../assets/images/calendar.png';
import Container from '@material-ui/core/Container';
import { Row, Column } from 'react-foundation';
import { fetch } from "frontity";

const Agenda_View = ({ state }) => {
  const data = state.source.get(state.router.link);
  console.log(data);
 
  
    return (
      <>
      <Container>
      <Section_title>
          <h2>Próximos Eventos</h2>
      </Section_title>
      <SectionContainer>
      <Descricao_Parceiros>
          <img width="100%" src={Logo_Img}/>
       
     <h2>THINK CONFERENCE 2020</h2>
     <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Calendar_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> 15 Junho 2020 </p>
   </Section_paragraph>
    </Contact_icons>
    <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Location_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> Porto, Portugal </p>
   </Section_paragraph>
    </Contact_icons>
    <p>
    Left fruit Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nobis saepe fuga sunt, atque ratione adipisci autem ducimus, quisquam sapiente magnam assumenda modi optio, officiis incidunt aut consequuntur commodi ipsa eos!

    </p>

    <input type="submit" value="Saber Mais" />
      </Descricao_Parceiros>
      <Descricao_Parceiros>
          <img width="100%" src={Logo_Img}/>
       
     <h2>THINK CONFERENCE 2020</h2>
     <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Calendar_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> 15 Junho 2020 </p>
   </Section_paragraph>
    </Contact_icons>
    <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Location_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> Porto, Portugal </p>
   </Section_paragraph>
    </Contact_icons>
    <p>
    Left fruit Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nobis saepe fuga sunt, atque ratione adipisci autem ducimus, quisquam sapiente magnam assumenda modi optio, officiis incidunt aut consequuntur commodi ipsa eos!

    </p>

    <input type="submit" value="Saber Mais" />
      </Descricao_Parceiros>
      <Descricao_Parceiros>
          <img width="100%" src={Logo_Img}/>
       
     <h2>THINK CONFERENCE 2020</h2>
     <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Calendar_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> 15 Junho 2020 </p>
   </Section_paragraph>
    </Contact_icons>
    <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Location_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> Porto, Portugal </p>
   </Section_paragraph>
    </Contact_icons>
    <p>
    Left fruit Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nobis saepe fuga sunt, atque ratione adipisci autem ducimus, quisquam sapiente magnam assumenda modi optio, officiis incidunt aut consequuntur commodi ipsa eos!

    </p>

    <input type="submit" value="Saber Mais" />
      </Descricao_Parceiros>

     
         
      </SectionContainer>
      </Container>
      <Container>
      <Section_title>
          <h2> Eventos Anteriores</h2>
      </Section_title>
      <SectionContainer>
      <Descricao_Parceiros>
          <img width="100%" src={Logo_Img}/>
       
     <h2>THINK CONFERENCE 2020</h2>
     <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Calendar_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> 15 Junho 2020 </p>
   </Section_paragraph>
    </Contact_icons>
    <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Location_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> Porto, Portugal </p>
   </Section_paragraph>
    </Contact_icons>
    <p>
    Left fruit Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nobis saepe fuga sunt, atque ratione adipisci autem ducimus, quisquam sapiente magnam assumenda modi optio, officiis incidunt aut consequuntur commodi ipsa eos!

    </p>

    <input type="submit" value="Saber Mais" />
      </Descricao_Parceiros>
      <Descricao_Parceiros>
          <img width="100%" src={Logo_Img}/>
       
     <h2>THINK CONFERENCE 2020</h2>
     <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Calendar_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> 15 Junho 2020 </p>
   </Section_paragraph>
    </Contact_icons>
    <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Location_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> Porto, Portugal </p>
   </Section_paragraph>
    </Contact_icons>
    <p>
    Left fruit Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nobis saepe fuga sunt, atque ratione adipisci autem ducimus, quisquam sapiente magnam assumenda modi optio, officiis incidunt aut consequuntur commodi ipsa eos!

    </p>

    <input type="submit" value="Saber Mais" />
      </Descricao_Parceiros>
      <Descricao_Parceiros>
          <img width="100%" src={Logo_Img}/>
       
     <h2>THINK CONFERENCE 2020</h2>
     <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Calendar_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> 15 Junho 2020 </p>
   </Section_paragraph>
    </Contact_icons>
    <Contact_icons>
         <Icons_layout>
         <img width="50%" src={Location_Icon}/> 
         </Icons_layout>
   <Section_paragraph>
   <p> Porto, Portugal </p>
   </Section_paragraph>
    </Contact_icons>
    <p>
    Left fruit Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nobis saepe fuga sunt, atque ratione adipisci autem ducimus, quisquam sapiente magnam assumenda modi optio, officiis incidunt aut consequuntur commodi ipsa eos!

    </p>

    <input type="submit" value="Saber Mais" />
      </Descricao_Parceiros>

     
         
      </SectionContainer>
      </Container>
    

      </>
    
    );
     
};

const maxWidths = {
    thin: "58rem",
    small: "80rem",
    medium: "100rem",
  };
  
  const getMaxWidth = (props) => maxWidths[props.size] || maxWidths["medium"];
  
  const SectionContainer = styled.div`
    margin-left: auto;
    margin-right: auto;
    width: calc(100% - 4rem);
    background-color:#fff;
    max-width: ${getMaxWidth};
    display:flex;
    @media (max-width: 800px) {
        width:90%;
        display:block;
        padding:15px;
      }
   
    
    
  `;

const Section_title = styled.div`
   width:90%;
    margin-left:50px;
    @media (max-width: 800px) {
        width:100%;
        margin-left:0px;
      }
   & > h2  {
    color: #000;
    margin-bottom: 10px;
    font-size: 26px;
    font-weight: 700;
    line-height: 1.4;
    letter-spacing: 0.5px;
    text-transform: uppercase;
   }
  
   

`;
const Contact_icons = styled.div`
   width:100%;
   display:flex;

 

`;

const Icons_layout = styled.div`
  margin-top:3%;
  width:20%;
  @media (max-width: 800px) {
    width:10%;
  }
`;
const Section_paragraph = styled.div`
   width:80%;
   
   & > p  {
    font-size: 14px!important;
    line-height: 20px!important;
    color: #000!important;
    font-weight:300!important;
    display: inline-block;
    text-transform: none;
    padding-left: 1px;
    margin-top:22px;
    position: relative;

   }
   

`;

const Descricao_Parceiros = styled.div`
   width:33.3%;
   text-align:left;
   padding:30px;
   margin-top:20px;
   & > p  {
    font-size: 14px!important;
    line-height: 20px!important;
    color: #000!important;
    font-weight:300!important;
    display: inline-block;
    text-transform: none;
    margin-top: 0px
   }
   @media (max-width: 800px) {
    width:100%;
    padding:0px;
  
  }

  input[type=submit] {
    display: inline-block;
    background: #25938a;
    border: 1px solid #25938a;
    color: #fff;
    padding: 12px 20px;
    margin: 0;
    width: auto;
    text-transform: uppercase;
    -webkit-transition: .3s;
    transition: .3s;
    cursor: pointer;
    font-size: 12px;
    margin-left:10px;
  
  }
}

   & > h2  {
    color: #25938a;
    font-size: 22px;
    font-weight: 700;
    line-height: 1.4;
    letter-spacing: 0.5px;
    text-transform: uppercase;
    margin: 0;
   }
   & > p  {
    font-family: 'Hind', sans-serif;
    font-size: 14px;
    font-weight: 300;
    line-height: 30px;
    position: relative;
    color: #000;
    margin-left: 10px;
    
}
`;



  export default connect(Agenda_View); 