import React from "react";
import { connect, styled } from "frontity";
import Logo_Img from '../../assets/images/blog-bg-1.jpg';
import Location_Icon from '../../assets/images/pin.png';
import Calendar_Icon from '../../assets/images/calendar.png';
import Container from '@material-ui/core/Container';
import { Row, Column } from 'react-foundation';
import {SocialMediaIconsReact} from 'social-media-icons-react';
const Contacts_View = ({ state }) => {
    return (
      <>
      <Container>
          <SectionContainer>
      <Section_Contact>
          <h5>Contactos</h5>
          <p><b>Telefone</b> </p>
          <p> <span>+351 910 000 000</span> </p>
          <p><b>Email</b></p>
          <p> <span><a href="">me@tiagoalmeidanogueira.com </a></span> </p>
          <p><b>Redes Sociais</b></p> 
          <SocialNetwork_icons>  
         <SocialMediaIconsReact borderColor="rgba(0,0,0,0.25)" borderWidth="0" borderStyle="solid" icon="facebook" iconColor="rgba(255,255,255,1)" backgroundColor="rgba(37,147,138,1)" iconSize="5" roundness="50%" url="https://some-website.com/my-social-media-url" size="40" />
          
        
             <b>
             <SocialMediaIconsReact borderColor="rgba(0,0,0,0.25)" borderWidth="0" borderStyle="solid" icon="instagram" iconColor="rgba(255,255,255,1)" backgroundColor="rgba(37,147,138,1)" iconSize="5" roundness="50%" url="https://some-website.com/my-social-media-url" size="40" />
             </b>
             <b>
             <SocialMediaIconsReact borderColor="rgba(0,0,0,0.25)" borderWidth="0" borderStyle="solid" icon="twitter" iconColor="rgba(255,255,255,1)" backgroundColor="rgba(37,147,138,1)" iconSize="5" roundness="50%" url="https://some-website.com/my-social-media-url" size="40" />
             </b>

             <b>
          <SocialMediaIconsReact borderColor="rgba(0,0,0,0.25)" borderWidth="0" borderStyle="solid" icon="skype" iconColor="rgba(255,255,255,1)" backgroundColor="rgba(37,147,138,1)" iconSize="5" roundness="50%" url="https://some-website.com/my-social-media-url" size="40" />
            </b>
          </SocialNetwork_icons>
                        
                                 
         
      </Section_Contact>
      <Section_Contact>
      <h5>Fale comigo!</h5>

      <input type="text" name="name" id="name" required="" placeholder="Nome"></input>
      <input type="email" name="email" id="email" required="" placeholder="E-mail"></input>
      <input type="text" name="subject" id="subject" placeholder="Assunto"></input>
      <input type="text" name="subject" id="subject" placeholder="Mensagem"></input>
     <input type="submit" name="Enviar"></input>
      </Section_Contact>
      </SectionContainer>
      </Container>
    

      </>
    
    );
     
};

const maxWidths = {
    thin: "58rem",
    small: "80rem",
    medium: "100rem",
  };
  
  const getMaxWidth = (props) => maxWidths[props.size] || maxWidths["medium"];
  
  const SectionContainer = styled.div`
    margin-left: auto;
    margin-right: auto;
    width: calc(100% - 4rem);
    background-color:#fff;
    max-width: ${getMaxWidth};
    display:flex;
    @media (max-width: 800px) {
        width:90%;
        display:block;
        padding:15px;
      }
   
    
    
  `;
  const SocialNetwork_icons = styled.div`
   width:70%;
   & >  b {
    margin-left:10px;

}
  
  
`;  

const Section_Contact = styled.div`
   width:50%;
   display:block;
   & > h5  {
       font-size:24px;
   }
   @media (max-width: 800px) {
    width:100%;
    display:block;
    
  }
   & > p b {
    font-size: 18px;
   color: #25938a;
   font-weight: 700;

}
& > p spam {
    display: block;
    font-size: 18px;
    color: #000;
}
input[type=text] {
    width: 100%;
    background: transparent;
    border: 1px solid #ddd;
    padding: 8px 15px;
    font-size: 14px;
    font-weight: 300;
    letter-spacing: 1px;
    -webkit-transition: .3s;
    transition: .3s;
    margin-bottom: 20px;
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid #ccc;
   
   }
   input[type=submit] {
    display: inline-block;
    background: #25938a;
    border: 1px solid #25938a;
    color: #fff;
    padding: 12px 20px;
    margin: 0;
    width: auto;
    text-transform: uppercase;
    -webkit-transition: .3s;
    transition: .3s;
    cursor: pointer;
    font-size: 12px;
  
  }
   input[type=email] {
    width: 100%;
    background: transparent;
    border: 1px solid #ddd;
    padding: 8px 15px;
    font-size: 14px;
    font-weight: 300;
    letter-spacing: 1px;
    -webkit-transition: .3s;
    transition: .3s;
    margin-bottom: 20px;
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid #ccc;
   }
   

   
`;


const Section_paragraph = styled.div`
   width:80%;
   
   & > p  {
    font-size: 14px!important;
    line-height: 20px!important;
    color: #000!important;
    font-weight:300!important;
    display: inline-block;
    text-transform: none;
    padding-left: 1px;
    margin-top:22px;
    position: relative;

   }
   

`;




  export default connect(Contacts_View); 