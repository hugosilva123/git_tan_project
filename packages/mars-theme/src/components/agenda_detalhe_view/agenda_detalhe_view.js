import React from "react";
import { connect, styled } from "frontity";
import Logo_Img from '../../assets/images/blog-bg-1.jpg';
import Location_Icon from '../../assets/images/pin.png';
import Calendar_Icon from '../../assets/images/calendar.png';
import Container from '@material-ui/core/Container';
import { Row, Column } from 'react-foundation';
import {SocialMediaIconsReact} from 'social-media-icons-react';
const Contacts_View = ({ state }) => {
    return (
      <>
      <Section_title>
      <Container>
         <Style_of_Title>
         <h1>THINK CONFERENCE 2020</h1>

         </Style_of_Title>
         
          
           </Container>
      </Section_title>
      <Container>
          <SectionContainer>
      <Section_Img>
        <img width="100%" src={Logo_Img}/>   
      </Section_Img>
      <Section_description>
        <Section_Data_Location>
        <Section_Calendar_Icon>
        <img width="40px" src={Calendar_Icon}/>   
        </Section_Calendar_Icon>
        <Style_Data_Location>
          <p>15 Junho 2020</p>
        </Style_Data_Location>
        </Section_Data_Location>
        <Section_Data_Location>
        <Section_Location_Icon>
        <img width="40px" src={Location_Icon}/>   
        </Section_Location_Icon>
        <Style_Data_Location>
          <h5>Porto,Portugal</h5>
        </Style_Data_Location>
        </Section_Data_Location>
        <p>Great adipisicing elit. Eaque maiores a iure numquam obcaecati recusandae omnis quos nihil
          velit, cum odit vero fugit dignissimos ipsum libero. Id inventore nostrum ab, dolor, fugit
          corrupti ducimus magni, quas vitae blanditiis eveniet voluptates.
         Pumpkin consectetur adipisicing elit. Voluptas, error rerum! Sed ducimus nihil est adipisci.
         Ut incidunt, ratione, perferendis rem dolores unde est quas blanditiis quo saepe sint.
        Facere nisi saepe ullam ab. Voluptates ducimus odit similique a quia.</p>
        <a href="/tan/agenda">Voltar Atrás</a>
      </Section_description>
      </SectionContainer>
      </Container>
    
     
      </>
    
    );
     
};

const maxWidths = {
    thin: "58rem",
    small: "80rem",
    medium: "100rem",
  };
  
  const getMaxWidth = (props) => maxWidths[props.size] || maxWidths["medium"];
  
  const SectionContainer = styled.div`
    margin-left: auto;
    margin-right: auto;
    width: calc(100% - 4rem);
    background-color:#fff;
    max-width: ${getMaxWidth};
    display:flex;
    @media (max-width: 800px) {
        width:90%;
        display:block;
        padding:15px;
      }
   
    
    
  `;
  const Section_title = styled.div`
   width:100%;
   height: 200px;
   margin-top:-20px;
   background:#25938a;
  
}
  
`;
const Style_of_Title = styled.div`
padding:50px;
@media (max-width: 1000px) {
  padding-top:50px;
  width:80%;
}
& > h1  {

 font-size: 34px;
 font-weight: 700;
 letter-spacing: 0.5px;
 color: #fff;
 line-height: 1.2;
 text-transform: uppercase;
}


`;   

const Section_Img = styled.div`
   width:50%;
   padding:30px;
   display:block;
   & > h5  {
       font-size:24px;
   }
   @media (max-width: 1000px) {
    width:90%;
    display:block;
    padding:15px;
  }

   
`;
const Section_Data_Location = styled.div`
   width:100%;
   display:flex;
   
 
`;
const Style_Data_Location = styled.div`
   width:80%;
   
  
& >  p {
    margin-top:40px;
    font-size: 14px!important;
    line-height: 20px!important;
    color: #000!important;
    font-weight: 300!important;
    display: inline-block;
    text-transform: none;
    padding-left: 21px;
    position: relative;
  
  }
  & >  h5 {
    font-size: 14px!important;
    line-height: 20px!important;
    color: #000!important;
    padding-bottom: 5px;
    font-weight: 300!important;
    display: inline-block;
    text-transform: none;
    padding-left: 21px;
    position: relative;
    padding-bottom: 20px;
  }  
   
 
`;
const Section_Calendar_Icon = styled.div`
   width:7%;
   margin-top:38px;
 
`;

const Section_Location_Icon = styled.div`
   width:7%;
   margin-top:10px;
 
`;


const Section_description = styled.div`
   width:50%;
   @media (max-width: 1000px) {
    width:90%;
    display:block;
    padding:15px;
  }
   
   & > p  {
    font-size: 14px!important;
    line-height: 20px!important;
    color: #000!important;
    font-weight:300!important;
    display: inline-block;
    text-transform: none;
    padding-left: 1px;
    margin-top:22px;
    position: relative;

   }
 & > a{
  display: inline-block;
  background: #25938a;
  color: #fff;
  font-size: 12px;
  font-weight: 500;
  letter-spacing: 1px;
  text-transform: capitalize;
  padding: 10px 20px;
  text-align: center;
  vertical-align: middle;
  cursor: pointer;
  border: 1px solid #25938a;
   :hover{
     color:#fff;
     
   }
 }  
   

`;




  export default connect(Contacts_View); 