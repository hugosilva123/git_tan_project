import React from "react";
import { connect, styled } from "frontity";
import Link from "./link";
import Nav from "./nav";
import MobileMenu from "./menu";
import Logo_Image from '../../src/assets/images/tan_logo.jpeg'
const Header = ({ state }) => {
  return (
    <>
      <Container>
      <Logo>    
         <img width="120" src={Logo_Image}></img>
       
         </Logo>
 
        <MobileMenu />
        <Nav />
         
      </Container>
   
    </>
  );
};

// Connect the Header component to get access to the `state` in it's `props`
export default connect(Header);

const Container = styled.div`
width: 100%;
max-width: 100%;

  padding: 0px;
  color: #fff;
  background-color:#0b0b0b;
  display: flex;
  heigth:72px;

`;

const Title = styled.h2`
  margin: 0;
  margin-bottom: 16px;
`;

const Description = styled.h4`
  margin: 0;
  color: rgba(255, 255, 255, 0.7);
`;


const Logo = styled.div`
  width: 20%;
  flex-direction: column;
  margin-top:3px;
  margin-left:20px;

`;