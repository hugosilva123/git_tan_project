import React from "react";
import { connect, styled } from "frontity";
import Parceiros_Img from '../../assets/images/salvador_caetano.png';
import Container from '@material-ui/core/Container';
import { Row, Column } from 'react-foundation';
const Parceiros_View = ({ state }) => {
    return (
      <>
      <Container>
      <SectionContainer>
      <Descricao_Parceiros>
     <h2>SALVADOR CAETANO</h2>
      <p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>
<p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>

      </Descricao_Parceiros>
      <Logo_Parceiros>
      <img width="100%" src={Parceiros_Img}/> 
      </Logo_Parceiros>
     
         
      </SectionContainer>
      </Container>
      <Container>
      <SectionContainer>
      <Descricao_Parceiros>
     <h2>SALVADOR CAETANO</h2>
      <p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>
<p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>

      </Descricao_Parceiros>
      <Logo_Parceiros>
      <img width="100%" src={Parceiros_Img}/> 
      </Logo_Parceiros>
     
         
      </SectionContainer>
      </Container>

      </>
    
    );
     
};

const maxWidths = {
    thin: "58rem",
    small: "80rem",
    medium: "100rem",
  };
  
  const getMaxWidth = (props) => maxWidths[props.size] || maxWidths["medium"];
  
  const SectionContainer = styled.div`
    margin-left: auto;
    margin-right: auto;
    width: calc(100% - 4rem);
    background-color:#fff;
    max-width: ${getMaxWidth};
    display:flex;

    @media (max-width: 800px) {
        width:100%;
        display:block;
      }
    
    
  `;

const Logo_Parceiros = styled.div`
   width:35%;
   margin-top:9%;
   @media (max-width: 800px) {
    width: 90%;
  } 
 

`;
const Descricao_Parceiros = styled.div`
   width:65%;
   text-align:left;
   padding:30px;
   @media (max-width: 800px) {
    width:100%;
    padding:0px;
    margin-top:30px;
  }
}

   & > h2  {
    color: #25938a;
    font-size: 26px;
    font-weight: 700;
    line-height: 1.4;
    letter-spacing: 0.5px;
    text-transform: uppercase;
    margin: 0;
   }
   & > p  {
    font-family: 'Hind', sans-serif;
    font-size: 14px;
    font-weight: 300;
    line-height: 30px;
    position: relative;
    color: #000;
    margin-left: 10px;
    
}
 

`;


  export default connect(Parceiros_View); 