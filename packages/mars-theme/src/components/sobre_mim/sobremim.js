import React from "react";
import { connect, styled } from "frontity";
import TAN_View from '../../assets/images/tan_img.jpg';
import Container from '@material-ui/core/Container';
import { Row, Column } from 'react-foundation';
const Sobre_View = ({ state }) => {
    return (
      <>
      <Container>
      <SectionContainer>
          <About>
          <Image_1>  <img width="100%" src={TAN_View}/></Image_1>
          <Const_about>  <h2>Sobre Tiago Almeida Nogueira</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>
          </Const_about>
          </About>
      </SectionContainer>
      </Container>

      <Container>
      <SectionContainer>
          <About>
          
          <Const_about_2>  <h2>Percurso Profissional</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>
          
          </Const_about_2>
          <Image_1>  <img width="100%" src={TAN_View}/></Image_1>
          </About>
     
         
      </SectionContainer>
      </Container>
      </>
    
    );
     
};

const maxWidths = {
    thin: "58rem",
    small: "80rem",
    medium: "100rem",
  };
  
  const getMaxWidth = (props) => maxWidths[props.size] || maxWidths["medium"];
  
  const SectionContainer = styled.div`
    margin-left: auto;
    margin-right: auto;
    width: calc(100% - 4rem);
    background-color:#fff;
    max-width: ${getMaxWidth};
  
    @media (min-width: 700px) {
      width: calc(100% - 8rem);
    }
  `;
  const About = styled.div`
  margin-top:3%;
  
  @media (min-width: 700px) {
      display: flex;
      width:100%;
    }

    @media (max-width: 800px) {
      
      width:100%;
  
    }  
  `;
const Image_1 = styled.div`
   width:50%;
   & > h2  {
    margin-top:24px;
    color: #25938a;
    font-size: 26px;
    font-weight: 700;
    line-height: 1.4;
    letter-spacing: 0.5px;
    text-transform: uppercase;
    margin: 0;
   }
   @media (max-width: 800px) {
    width: 100%;
  } 
 

`;
const Const_about = styled.div`
   width:50%;
   text-align:left;
   margin-left:3%;
   padding:30px;
   & > h2  {
    color: #25938a;
    font-size: 26px;
    font-weight: 700;
    line-height: 1.4;
    letter-spacing: 0.5px;
    text-transform: uppercase;
    margin: 0;
   }
   & > p  {
    font-family: 'Hind', sans-serif;
    font-size: 14px;
    font-weight: 300;
    line-height: 30px;
    position: relative;
    color: #000;
    margin-left: 10px;
    
}
@media (max-width: 800px) {
    width: 100%;
    margin-left:0%;
    padding:0px;
} 

`;

const Const_about_2 = styled.div`
   width:50%;
   text-align:left;
   margin-top:10px;
   margin-right:3%;
   & > h2  {
    color: #25938a;
    font-size: 26px;
    font-weight: 700;
    line-height: 1.4;
    letter-spacing: 0.5px;
    text-transform: uppercase;
    margin: 0;
   }
   & > p  {
    font-family: 'Hind', sans-serif;
    font-size: 14px;
    font-weight: 300;
    line-height: 30px;
    position: relative;
    color: #000;
    margin-left: 10px;
}

@media (max-width: 800px) {
  width: 100%;
  margin-left:0%;
  padding:0px;
} 
`;
  export default connect(Sobre_View); 