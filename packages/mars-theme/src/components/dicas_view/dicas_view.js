import React from "react";
import { connect, styled } from "frontity";
import Logo_Img from '../../assets/images/blog-bg-1.jpg';
import Location_Icon from '../../assets/images/pin.png';
import Calendar_Icon from '../../assets/images/calendar.png';
import Container from '@material-ui/core/Container';
import { Row, Column } from 'react-foundation';

const Dicas_View = ({ state }) => {
    return (
      <>
      <Container>
      <SectionContainer>

      <Dicas_Container>
          <Logo_Container>
          <img width="100%" src={Logo_Img}/>
          </Logo_Container>
         <Dicas_Description>
         <span><a href="#">Marketing Digital</a></span>
         <span><a href="#">E-commerce</a></span>
         <h3><a href="/tan/dicas_detalhe">Como ganhar seguidores no Instagram e criar um perfil autoritário</a></h3>
         <span>Maio 20, 2020</span>
         <p>Left fruit Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nobis saepe fuga sunt,
                                        atque ratione adipisci autem ducimus, quisquam sapiente magnam assumenda modi optio,
                                        officiis incidunt aut consequuntur commodi ipsa eos!</p>
        <a href="/tan/dicas_detalhe" class="main-btn">Ler Mais</a>                         
         </Dicas_Description>
       
      </Dicas_Container>
      
      <Form_Section>
       <h5>Categorias</h5>
       <a href="#">Marketing Digital (8)</a>
       <a href="#">E-commerce (8)</a>
       <a href="#">E-mail Marketing (8)</a>
       <a href="#">Marketing Digital (8)</a>
       <a href="#">E-commerce (8)</a>
      <a href="#">E-mail Marketing (8)</a>
      <h5>Tags</h5>
      <span><a href="#">marketing</a></span>
      <span><a href="#">digital</a></span>
      <span><a href="#">e-commerce</a></span>
      <span><a href="#">redes sociais</a></span>

   </Form_Section>
      </SectionContainer>
      </Container>
      </>
    
    );
     
};

const maxWidths = {
    thin: "58rem",
    small: "80rem",
    medium: "100rem",
  };
  
  const getMaxWidth = (props) => maxWidths[props.size] || maxWidths["medium"];
  
  const SectionContainer = styled.div`
    margin-left: auto;
    margin-right: auto;
    width: calc(100% - 4rem);
    background-color:#fff;
    max-width: ${getMaxWidth};
    display:flex;
    @media (max-width: 1000px) {
        width:100%;
        display:block;
        
      }
  `;

const Contact_icons = styled.div`
   width:100%;
   display:flex;
 
`;
const Form_Section = styled.div`
   width:20%;
   text-align:left;
   display:block;
   margin-top:20px;
   @media (max-width: 1000px) {
    width: 90%;
    display: block;
    margin-left: auto;
    margin-right: auto
 
}
  
   
   & >  h5 {
   font-size: 18px;
    }
   & >  a {
     display:block;
     color: #000;
    font-size: 13px;
    margin-bottom: 15px;
   }

   & > p  {
    font-size: 14px!important;
    line-height: 20px!important;
    color: #000!important;
    font-weight:300!important;
    display: inline-block;
    text-transform: none;
    margin-top: 0px
   }
  & > span a  {
    padding: 8px 15px;
    background: #25938a;
    margin-top:10px;
    display:block;
    max-width:30%;
    color: #fff;
    margin-left:20px;
    font-size: 11px;
    border: 1px solid #25938a;
    @media (max-width: 1000px) {
        margin-left:0px;
     
    }
   }
`;
const Logo_Container = styled.div`
   width:30%;
   text-align:left;
   padding:30px;
   margin-top:20px;
  
   @media (max-width: 1000px) {
    width:100%;
    display:block;
    padding:0px;
  
  }
  `;

  const Dicas_Description = styled.div`
  width:70%;
  text-align:left;
  padding-left:5px;
  @media (max-width: 1000px) {
    width:90%;
    display:block;
    text-align: justify;
  }

  margin-top:40px;
  & >  a {
    padding: 8px 15px;
    background: #25938a;
    color:#fff;
    margin-bottom: 10px;
    margin-right: 10px;
    font-size: 11px;
    border: 1px solid #25938a;  
  }


  & > span a {
    font-family: 'Hind', sans-serif;
    font-size: 14px;
    font-weight: 400;
    position: relative;
    z-index: 1;
    background: #fff;
    color: #000;
  
    text-decoration:underline;
   }

   & > span  {
    font-family: 'Hind', sans-serif;
    font-size: 10px;
    font-weight: 400;
    position: relative;
    z-index: 1;
    background: #fff;
    color: #000;
   }  

   & > h3 a {
    margin-bottom: 0;
    font-weight: 700;
    font-size: 22px;
    text-transform: uppercase;
    color: #25938a;
    line-height: 24px;  
   }

 `;  

const Dicas_Container = styled.div`
   width:80%;
   text-align:left;
   border-bottom: 1px  #f0f0f5 solid ;
   padding:30px;
   margin-top:20px;
   display:flex;
   @media (max-width: 1000px) {
    width:90%;
    display:block;
    margin-left:auto;
    margin-right:auto;
  
  }
   

   & > p  {
    font-size: 14px!important;
    line-height: 20px!important;
    color: #000!important;
    font-weight:300!important;
    display: inline-block;
    text-transform: none;
    margin-top: 0px
   }

  input[type=submit] {
    display: inline-block;
    background: #25938a;
    border: 1px solid #25938a;
    color: #fff;
    padding: 12px 20px;
    margin: 0;
    width: auto;
    text-transform: uppercase;
    -webkit-transition: .3s;
    transition: .3s;
    cursor: pointer;
    font-size: 12px;
    margin-left:10px;
  
  }
}

   & > h2  {
    color: #25938a;
    font-size: 22px;
    font-weight: 700;
    line-height: 1.4;
    letter-spacing: 0.5px;
    text-transform: uppercase;
    margin: 0;
   }
   & > p  {
    font-family: 'Hind', sans-serif;
    font-size: 14px;
    font-weight: 300;
    line-height: 30px;
    position: relative;
    color: #000;
    margin-left: 10px;
    
}
`;



  export default connect(Dicas_View); 