import React from "react";
import { Global, css, connect, styled, Head } from "frontity";
import Switch from "@frontity/components/switch";
import Header from "./header";
import List from "./list";
import Post from "./post";
import Loading from "./loading";
import Title from "./title";
import PageError from "./page-error";
import Home_View from "./Home_View/home_view";
import Footer from "./Footer/footer";
import Base_View from "./base_view/base_view";
import Sobre_Mim from "./sobre_mim/sobremim";
import Parceiros_View from "./parceiros_view/parceiros_view";
import Ferramentas_View from "./Ferramentas_View/ferramentas_view";
import Agenda_View from "./Agenda_View/agenda_view";
import Dicas_View from "./dicas_view/dicas_view";
import Contact_View from "./contacts_view/contacts_view";
import Agenda_detalhe_View from "./agenda_detalhe_View/agenda_detalhe_View";
import { fetch } from "frontity";
/**
 * Theme is the root React component of our theme. The one we will export
 * in roots.
 */
const Theme = ({ state }) => {
  // Get information about the current URL.
 
  const data = state.source.get(state.router.link);
  
  return (
    <>
      {/* Add some metatags to the <head> of the HTML. */}
      <Title />
      <Head>
        
        <meta name="description" content={state.frontity.description} />
        <html lang="en" />
      </Head>

      {/* Add some global styles for the whole site, like body or a's. 
      Not classes here because we use CSS-in-JS. Only global HTML tags. */}
      <Global styles={globalStyles} />

      {/* Add the header of the site. */}
      <HeadContainer>
        <Header />
      </HeadContainer>
      {/* Add the main section. It renders a different component depending
      on the type of URL we are in. */}
      <Main>
        <Switch>
        <Home_View when={data.isHome}/>
        <Loading when={data.isFetching} />
        <Ferramentas_View when={data.isSignUp}/>
          <List when={data.isArchive} />
          <Post when={data.isPostType} />
          <PageError when={data.isError} />
        </Switch>
      </Main>
    <Footer/>
    </>
  );
};

export default connect(Theme);

const globalStyles = css`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
      "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  }
  a,
  a:visited {
    color: inherit;
    text-decoration: none;
  }
`;

const HeadContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: #1f38c5;
`;

const Main = styled.div`
  width:100%;

`;
