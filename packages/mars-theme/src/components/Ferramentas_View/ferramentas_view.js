import React, {useEffect} from "react";
import { connect, styled } from "frontity";
import Wordpress_logo from '../../assets/images/wordpress.png';
import Container from '@material-ui/core/Container';
import { Row, Column } from 'react-foundation';
import { fetch } from 'frontity';
import List from '../link';
import Item from "../list/list-item";


 const fetchToken = async  ({ state,data }) => {
      const res = await fetch(
        `http://tan.mybrontley.com/wp-json/bspcustom/getposts?post_type=ferramentas/`, {
            method: 'GET',
            headers:  {
              'Content-Type': 'application/json',
              'Authorization':'Basic YWRtLXRhbjA5MzI6R21wUFBhN295TUdZQ1BrRnVN'
            },
            redirect: 'follow'
        }
    );
     const body = await res.json();
    return body;
      }
const Ferramentas_View = ({ state,actions }) => {
  const data =fetchToken(state);
z
    return (
      <>
<SectionContainer>
    <Container>
    
    <Main_Menu>
          <ul>
            <li><a href="#">  </a></li>
            <li><a href="#">Redes Sociais </a></li>
            <li><a href="#">E-mailMarketing </a></li>
                         
          </ul>
         </Main_Menu>
          </Container>
         
      </SectionContainer>
      
          <Container>
          <Section_Ferramentas>
              <Logo><img src={Wordpress_logo}/> </Logo>
              <Descrition><h2></h2>
              <p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>

<p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>
              
              </Descrition>
              </Section_Ferramentas>
          </Container>
          <Container>
          <Section_Ferramentas>
              <Logo><img src={Wordpress_logo}/> </Logo>
              <Descrition><h2></h2>
              <p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>

<p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>
              
              </Descrition>
              </Section_Ferramentas>
          </Container>
          <Container>
          <Section_Ferramentas>
              <Logo><img src={Wordpress_logo}/> </Logo>
              <Descrition><h2>WordPress</h2>
              <p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>

<p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim velit lorem, vestibulum ornare sem efficitur vitae. Ut eleifend leo vel eleifend efficitur. Cras malesuada convallis enim et tempus. Proin dolor quam, eleifend nec odio vitae, pulvinar sodales sapien.</p>
            
              </Descrition>
              </Section_Ferramentas>
          </Container>
       
      </>
    
    );
     
};


  
  const getMaxWidth = (props) => maxWidths[props.size] || maxWidths["medium"];
  
  const SectionContainer = styled.div`
  
    width: 100%;
    background-color: #25938a;
    max-width: 100%;
    display:flex;
    margin-top:-3px;
    
   
  `;

  const Main_Menu = styled.div`
  
  width: 100%;
  background-color: #25938a;
  max-width: 100%;
  display:flex;
  & > ul li  {
      list-style: none;
      margin-top:-80px;
      padding-left: 15px;
      display: inline;
      color:#fff;
      :hover{
        text-decoration: underline;
      }
     }

  @media (max-width: 800px) {
      width:100%;
      display:block;
    }
  
  
`;

const Section_Ferramentas = styled.div`
 width: 100%;
 display:flex;
 @media (max-width: 800px) {
  width:100%;
  display:block;
}
`;
const Logo  = styled.div`
width: 25%;
padding:5%;
@media (max-width: 800px) {
  width:100%;
  display: block;
  padding:0;
  text-align: center;
  margin-top:20px;
  
}
`;

const Descrition  = styled.div`
width: 70%;
@media (max-width: 800px) {
  width:100%;

}

& > h2  {
  color: #25938a;
  font-size: 26px;
  font-weight: 700;
  line-height: 1.4;
  margin-top:30px;
  letter-spacing: 0.5px;
  text-transform: uppercase;
  @media (max-width: 800px) {
    text-align:center;
  
  }
 
 }
 & > p  {
  font-family: 'Hind', sans-serif;
  font-size: 14px;
  font-weight: 300;
  line-height: 30px;
  position: relative;
  color: #000;
  margin: 0;
 
 }
`;


  export default connect(Ferramentas_View); 