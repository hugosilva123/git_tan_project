import React from "react";
import { styled, connect } from "frontity";
import Link from "../link";
import SectionContainer from "./section-container";
import { Row, Column } from 'react-foundation';
import { Container } from "@material-ui/core";
import Phone_logo from '../../assets/images/phone.png';
import Mail_logo from '../../assets/images/email.png';
import {SocialMediaIconsReact} from 'social-media-icons-react';
// Component that provides scroll to top functionality
const BackToTop = () => {
  // scroll to top function
  const scrollToTop = (event) => {
    // prevent the default behaviors
    event.preventDefault();
    // scroll to the top smoothly
    scrollTo({ top: 0, left: 0, behavior: "smooth" });
  };

  return (
    <a href="#site-header" onClick={scrollToTop} style={{ cursor: "pointer" }}>
      <span style={{ marginRight: 8 }}>To the top</span>
      <span className="arrow" aria-hidden="true">
        ↑
      </span>
    </a>
  );
};

const Footer = ({ state }) => {
  return (
  
    <SiteFooter  role="contentinfo">
      <SiteFooterInner>
      <Container>
      
        <Credits>
        <Rows_>
          <Row>
          <Column>
          <Column>
      <Title_color>
          Mapa do Site
          </Title_color>
          </Column>
          <Copyright>
          Home
          </Copyright>
          
          </Column>
          <Column> 
          <Copyright>
          Sobre mim
          </Copyright></Column>
          <Column> <Copyright>
          Parceiros
          </Copyright></Column>
          <Column><Copyright>
          Agenda
          </Copyright> </Column>
          <Column> <Copyright>
          Dicas
          </Copyright></Column>
          <Column><Copyright>
          Ferramentas
          </Copyright> </Column>
          <Column><Copyright>
          Contactos
          </Copyright> </Column>
          <Column><Copyright>
          
          </Copyright> 
          </Column>
          </Row>
          </Rows_>
          <Rows_>
          <Row>
          <Column>
          <Copyright>
          <Title_color>
          CONTACTOS
          </Title_color>
          </Copyright>
           </Column>
           <Column>
           <img width="22px" overflow="hidden"src={Phone_logo}></img>
           <Copyright_fone>
           +351 220 993 488
          </Copyright_fone>
           </Column>
           <Column>
           <Column></Column>
         
           <img width="25px"src={Mail_logo}></img>
          
           <Copyright_fone>
           me@tiagoalmeidanogueira.com
          </Copyright_fone> 
           

           </Column>


          </Row>
          </Rows_>
          <Rows_>
          <Row>
          <Column><Copyright>
          <Title_color>
          FIQUE LIGADO!
          </Title_color> 
          </Copyright> </Column>
          <Column>
          <Div_socialicons>
          <SocialMediaIconsReact borderColor="rgba(239,229,229,0.79)" borderWidth="1" borderStyle="solid" icon="facebook" iconColor="rgba(255,255,255,1)" backgroundColor="rgba(28,186,223,0)" iconSize="3" roundness="47%" url="https://some-website.com/my-social-media-url" size="33" />
          
          <SocialMediaIconsReact borderColor="rgba(239,229,229,0.79)" borderWidth="1" borderStyle="solid" icon="instagram" iconColor="rgba(255,255,255,1)" backgroundColor="rgba(28,186,223,0)" iconSize="3" roundness="47%" url="https://some-website.com/my-social-media-url" size="33" />
          <SocialMediaIconsReact  borderColor="rgba(239,229,229,0.79)" borderWidth="1" borderStyle="solid" icon="linkedin" iconColor="rgba(255,255,255,1)" backgroundColor="rgba(28,186,223,0)" iconSize="3" roundness="47%" url="https://some-website.com/my-social-media-url" size="33" />
          </Div_socialicons>
          </Column>
          </Row>



          </Rows_>
          <Rows_>
          <Row>
          <Column><Copyright>
          <Title_color>
          NEWSLETTER
          </Title_color>
        
          </Copyright> </Column>

          <Column>
          <Input_Form>
          <form>
         <label>
       <input type="text" placeholder="Nome"  name="name"/>  
       </label>
       <label>
       <input type="text" placeholder="Email"   name="name" />
       </label>
     <input type="submit" value="Subscrever" />
      </form>
  </Input_Form>
  </Column>

          </Row>
          </Rows_>
        </Credits>
      
        </Container>

      </SiteFooterInner>
      <Footer_Botton>
      <Container>
      <Section_Copyright>
      <Cop_right>
      <p>&copy; 2020 Tiago Almeida Nogueira. Todos os Direitos Reservados</p>
   
   <ul>
     <li><a href="#">Termos e Condições</a></li>
     <li><a href="#">Resolução Alternativa de Litígios</a></li>
     <li><a href="#">Livro de Reclamações</a></li>
   </ul>
      </Cop_right>
      <Link_Basicamente>
      <p><a href="https://basicamente.pt/">Basicamente.pt</a></p>
      </Link_Basicamente>
   
 
      </Section_Copyright>
     
        </Container>
        </Footer_Botton>  
    </SiteFooter>
   
  );
};

export default connect(Footer);
const maxWidths = {
  thin: "58rem",
  small: "80rem",
  medium: "100rem",
};

const getMaxWidth = (props) => maxWidths[props.size] || maxWidths["medium"];
const SiteFooterInner = styled(SectionContainer)`
  align-items: baseline;
  display: flex;
  justify-content: space-between;
  margin-left:50px;
 
 
`;

const Footer_Botton = styled.div`
margin-left: auto;
margin-right: auto;
width: 100%;
background-color:#000;
border-top: 0.5px  #f0f0f5 solid ;



@media (min-width: 700px) {
  width: 100%;
}
@media (max-width: 500px) {
  margin-top:20px;
}

`;

const SiteFooter = styled.footer`
  margin-top: 5rem;
  border-color: #dcd7ca;
  border-style: solid;
  border-width: 0;
  padding: 3rem 0;
  font-size:20px;
  background-color: #000;
  color: #fff;

  @media (min-width: 600px) {
    margin-botton: 8rem;
    font-size: 1.8rem;
  }
 
`;

const Credits = styled.div`
  
@media (min-width: 700px) {
    display: flex;
    width:100%;

  }
`;
const Section_Copyright = styled.div`
  
@media (min-width: 700px) {
    display: flex;
    width:100%;

  }
`;
const Rows_ = styled.div`
margin-left:3%;
width:25%;
  @media (max-width: 700px) {
    margin-top:7%;
    width:100%;
    text-align:center;
  }
  @media (max-width: 500px) {
    margin-left:0%;
  
  }

`;

const Copyright = styled.text`
  font-weight: 600;
  margin: 3%;
  font-size: 13px;
  text-transform:uppercase;
  @media (min-width: 700px) {
    font-weight: 700;
  }
`;
const Copyright_fone = styled.text`
  margin-left:3%;
  font-size: 13px;
  @media (min-width: 700px) {
    font-weight: 700;
  }
  @media (max-width: 500px) {
    margin-top:5%;
  }
`;

const Para_grafh = styled.div`
 width:100%;
 display:flex;

`;



const Title_color= styled.text`
 color:#25938a;
 font-size: 13px;
 text-transform:uppercase;
`;
const Base_pt= styled.text`
 float:right;

`;

const Div_socialicons = styled.div`
margin-left:1%;
& > a{
 margin-left:4%;
 margin-top:2%;
 :hover {
  color: #25938a;
  cursor: pointer;
}
}
  @media (min-width: 700px) {
    display: flex;
  }
`;
const Input_Form = styled.div`
margin-left:1%;
 input[type=text] {
 width:100%;
 border: 3px;
 text-color:#fff;
 background: transparent;
 border-bottom: 2px solid #FFF;
 margin-bottom: 20px;

}
input[type=submit] {
  display: inline-block;
  background: #25938a;
  border: 1px solid #25938a;
  color: #fff;
  padding: 12px 20px;
  margin: 0;
  width: auto;
  text-transform: uppercase;
  -webkit-transition: .3s;
  transition: .3s;
  cursor: pointer;
  font-size: 12px;

}

  @media (min-width: 700px) {
    display: flex;
  }
`;
const Cop_right = styled.div`
 width:50%;
 @media (max-width: 700px) {
  width:100%;
  text-align:center;
  float:none;
}
& > ul  {
  list-style: none;
  margin-top:-80px;
  padding: 0;
  display: inline;
 }

 & > ul li  {
  list-style: none;
  margin-top:-80px;
  padding: 0;
  margin-left:20px;
  display: inline;
 }

& > ul li a {
  display: inline;
  padding-left: 0px;
  color: #fff;
  font-size: 12px;
;
  text-transform: none;
   :hover{
    text-decoration: underline;
  }
 }
& > p{
  margin: 3%;
  font-size: 12px;
  text-transform: none;
 }


`;
const Link_Basicamente = styled.div`
@media (max-width: 700px) {
    width:100%;
    text-align:center;
    float:none;
  }
width:50%;
 & > p a{
  float:right;
  margin: 3%;
  font-size: 12px;
  text-transform: none;
  @media (max-width: 700px) {
    width:100%;
    text-align:center;
    float:none;
  }

 }
 

  
`;

