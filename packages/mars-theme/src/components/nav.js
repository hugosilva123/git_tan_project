import React from "react";
import { connect, styled } from "frontity";
import Link from "./link";

/**
 * Navigation Component
 *
 * It renders the navigation links
 */
const Nav = ({ state }) => (
  <NavContainer>
    {state.theme.menu.map(([name, link]) => {
      // Check if the link matched the current page url
      const isCurrentPage = state.router.link === link;
      return (
        <NavItem key={name}>
          {/* If link url is the current page, add `aria-current` for a11y */}
          <Link link={link} aria-current={isCurrentPage ? "page" : undefined}>
            {name}
          </Link>
        
        </NavItem>
      );
    })}
  </NavContainer>
);

export default connect(Nav);


const NavContainer = styled.nav`

  list-style: none;
  display: flex;
  width: 848px;
  max-width: 100%;
  box-sizing: border-box;
 margin-left: 60px;
  margin-top: 20px;
 text-transform: uppercase;
 font-family: 'Hind', sans-serif;
 font-size:14px;
 font-weight: 2;
  @media screen and (max-width: 991px) {
    display: none;
  }
`;
const NavItem = styled.div`
  padding: 0;
  margin: 0 16px;
  color: #fff;
  font-size: 0.9em;
  box-sizing: border-box;
  flex-shrink: 0;
  :hover {
		color: #25938a;
		cursor: pointer;
	}
  & > a {
    display: block;
    line-height: 2em;
    border-bottom: 2px solid;
    
    border-bottom-color: transparent;
    /* Use for semantic approach to style the current link */
    &[aria-current="page"] {
      color:#25938a;
    }
  }

  &:first-of-type {
    margin-left: 0;
  }
  & > a {

  } 

  &:last-of-type {
    margin-right: 0;

    &:after {
      content: "";
      display: inline-block;
      width: 14px;
    }
  }
`;

