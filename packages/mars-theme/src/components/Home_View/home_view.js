import React from "react";
import { connect, styled } from "frontity";
import TAN_View from '../../assets/images/tan_img_2.jpg';
import Container from '@material-ui/core/Container';
import { Row, Column } from 'react-foundation';
import Post from '../../components/post';
import Post_Home from "./item_agenda_view";
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
//Grid Function...
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));



const Home_View = ({ state }) => {
  const data = state.source.get(state.router.link);
  const classes = useStyles();
  console.log(data);
    return (
      <>
      
       <img width="100%" overflow="hidden"src={TAN_View}/>
       <Sub_Email>
       <Container >
       <Container_Email>
       <Sub_Email_Section_Left>
       <h3>RECEBA AS MINHAS NOVIDADES NO SEU EMAIL!</h3>  
       </Sub_Email_Section_Left>
       <Sub_Email_Section_Right>
        <form>
      <label>
 
      <input type="text"  name="name"/>  
      </label>
      <label>
 
      <input type="text"  placeorder="E-mail"name="email" />
      </label>
      <input type="submit" value="Submeter" />
        </form>
       </Sub_Email_Section_Right>
       </Container_Email>
       </Container>
         </Sub_Email>

         
       <Imagem_Destaque> 
          <h1>Tiago Almeida Nogueira</h1>
         <p>Digital Marketing Specialist</p>
         </Imagem_Destaque>
         <Container>
        <div className={classes.root}>
        <Grid container spacing={1}>
        <Grid container item xs={12} spacing={3}>
        {data.items.map(({ type, id }) => {
        const item = state.source[type][id];
        // Render one Item component for each one.
        return <Grid item xs={12} md={4}> <Post_Home key={item.id} item={item} /> </Grid>;
          })}
        </Grid>
       </Grid>
    </div>
    </Container>
      </>
    
    );
     
};
  const Imagem_Destaque = styled.text`
   & > h1 {
    font-size: 52px;
    color: #fff;
    margin-left:13%;

    transform: translateY(-80%);
    font-weight: 700;
    margin-top:-25%;
    line-height: 1.4;
    @media ( max-width: 700px) {
        display: none;
        align-items: center;
        justify-content: center;
      }
}
& > p {
    margin-left:13%;
    font-size: 18px;
    line-height: 1.2;
    color: #fff;
    margin-top:-5%;
    font-weight: 300;
    @media ( max-width: 700px) {
        display: none;
        align-items: center;
        justify-content: center;
        margin-bottom:100px;   
      }
   margin-bottom:25%;   
}
`;

const Sub_Email = styled.div`
  width:100%;
 margin-top:-3%;
  display:flex;
  background-color:#25938a; 
  @media ( max-width: 581px) {
    margin-top:-6%;
   margin-bottom:100px;
  
  }
  
  @media ( max-width: 681px) {
   
   margin-bottom:100px;
  
  } 
  & > h3{
    color: #fff;
    font-weight: 400;
    font-size: 16px;
    line-height: 1;
    text-transform: uppercase;
  }
`;
const Container_Email = styled.div`
  width:100%;
  margin-top:43px;
  display:flex;
  background-color:#25938a; 
  @media ( max-width: 781px) {
    display:block;
  
  }

`;
const Sub_Email_Section_Left = styled.div`
  width:50%;
 justify-content:center;
  text-align:center;
  display:flex;
  background-color:#25938a; 
  @media ( max-width: 781px) {
    width:100%;
    display:block;
  } 
  & > h3{
      padding: 30px;
      color: #fff;
      font-weight: 400;
      font-size: 16px;
      line-height: 1;
      text-transform: uppercase;
      font-family: 'Hind', sans-serif;
      @media ( max-width: 781px) {
        padding: 15px;
      } 
    }
  }
`;
const Container_Inside = styled.div`
overflow: hidden;
  @media ( max-width: 781px) {
    width:100%;
  }

  
 
`;



const Sub_Email_Section_Right = styled.div`
  width:50%;
 justify-content:center;
  text-align:center;
  display:flex;
  background-color:#25938a; 
  @media ( max-width: 781px) {
    width:100%;
  
  }
  input[type=submit] {
    margin-top:25px;
    font-size: 14px;
    background-color: #fff;
    padding: 15px 15px;
    color: #25938a;
    border: 1px solid #fff;
    text-transform: uppercase;
    margin-left:20px;
  } 
  input[type=text] {
    margin-top:25px;
    border: 3px;
    text-color:#fff;
    margin-left:12px;
    background: transparent;
    border-bottom: 2px solid #FFF;
    @media ( max-width: 700px) {
      width:100%;
    }
  }
  & > h3{
    
    color: #fff;
    font-weight: 400;
    font-size: 16px;
    line-height: 1;
    text-transform: uppercase;
  }
`;

const Form_email = styled.div`
margin-top:-87px;
padding-bottom: 30px;
padding-left:60px;
margin-left:100px;
text-color:#fff;
input[type=submit] {
  font-size: 14px;
  background-color: #fff;
  padding: 15px 15px;
  color: #25938a;
  border: 1px solid #fff;
  text-transform: uppercase;
  margin-left:20px;
}
@media ( max-width: 411px) {
  margin-top:0%;
  margin-left:10px;  
 text-align:center;
 input[type=submit] {
 margin-top:20px; 
}
} 
@media ( max-width: 511px) {
  margin-top:0%;
  margin-left:-11%;
  padding-top:2%;
  button[type="submit"] {
    font-size: 14px;
    background-color: #fff;
    padding-top: 15px ;
    color: #25938a;
    border: 1px solid #fff;
  }
}
button[type="submit"] {
  font-size: 14px;
  background-color: #fff;
  padding: 15px 15px;
  color: #25938a;
  border: 1px solid #fff;
}
& > input {
 text-color:#fff;
}
input[type=text] {
  border: 3px;
  text-color:#fff;
  margin-left:12px;
  background: transparent;
  border-bottom: 2px solid #FFF;
}

input[type=value]{
  text-color:#fff;
}

`;



  export default connect(Home_View); 