import React from "react";
import { connect, styled } from "frontity";
import Link from "../link";
import FeaturedMedia from "../featured-media";
import Logo_Img from '../../assets/images/blog-bg-1.jpg';

/**
 * Item Component
 *
 * It renders the preview of a blog post. Each blog post contains
 * - Title: clickable title of the post
 * - Author: name of author and published date
 * - FeaturedMedia: the featured image/video of the post
 */
const Post_Home = ({ state, item }) => {
  const author = state.source.author[item.author];
  const date = new Date(item.date);
  
  return (
    <article>
         <img width="100%" src={Logo_Img}/>   
   
        <Title dangerouslySetInnerHTML={{ __html: item.title.rendered }} />


      <div>
        {/* If the post has an author, we render a clickable author text. */}
        {author && (
          <StyledLink link={author.link}>
          </StyledLink>
        )}
        <PublishDate>
         {date.toDateString()}
        </PublishDate>
      </div>

      {/*
       * If the want to show featured media in the
       * list of featured posts, we render the media.
       */}
      {state.theme.featured.showOnList && (
        <FeaturedMedia id={item.featured_media} />
      )}

      {/* If the post has an excerpt (short summary text), we render it */}
      {item.description && (
        <Excerpt dangerouslySetInnerHTML={{ __html: item.description }} />
      )}
      <Botton_Submit>
<a href={item.link}><input type="submit" value="Saber Mais" /></a>
     </Botton_Submit>
    </article>
  );
};

// Connect the Item to gain access to `state` as a prop
export default connect(Post_Home);

const Title = styled.h1`
    color: #25938a;
    font-size: 22px;
    font-weight: 700;
    line-height: 1.4;
    letter-spacing: 0.5px;
    text-transform: uppercase;
    margin: 0;
 
`;

const AuthorName = styled.span`
  color: rgba(12, 17, 43, 0.9);
  font-size: 0.9em;
`;

const StyledLink = styled(Link)`
  padding: 15px 0;
`;

const PublishDate = styled.span`
  font-size: 11px;
  color: #000;
  font-weight: 300;
`;
const Botton_Submit = styled.div`
input[type=submit] {
    display: inline-block;
    background: #25938a;
    border: 1px solid #25938a;
    color: #fff;
    padding: 12px 20px;
    margin: 0;
    width: auto;
    text-transform: uppercase;
    -webkit-transition: .3s;
    transition: .3s;
    cursor: pointer;
    font-size: 12px;
    
  
  }

`;
const Excerpt = styled.div`
  line-height: 1.6em;
  color: rgba(12, 17, 43, 0.8);
`;
