import Theme from "./components";
import image from "@frontity/html2react/processors/image";
import iframe from "@frontity/html2react/processors/iframe";
import { fetch } from 'frontity';

const signUpHandler = {
  pattern: "/ferramentass/",
  func: ({ state }) => {
    state.source.data["/ferramentass/"].isSignUp = true;
    //state.source.api.header=
  }
}

export const FerramentasHandler = {
  name: "ferramentas",
  pattern: "/ferramentas/",
  func: async ({ route, params, state, libraries }) =>
   { 
    const response = await libraries.source.api.get({ endpoint:"/bspcustom/getposts?post_type=ferramentas/" });{
    }
    console.log(response);
  }
}
/*export const fetchToken = async ({ state }) => {
  const res = await fetch(
      `http://tan.mybrontley.com/wp-json/bspcustom/getposts?post_type=ferramentas/`, {
          method: 'GET',
          headers:  {
            'Content-Type': 'application/json',
            'Authorization':'Basic YWRtLXRhbjA5MzI6R21wUFBhN295TUdZQ1BrRnVN'
          },
          redirect: 'follow'
      }
  );
   const body = await res.json();
   console.log(body)
}*/

const marsTheme = {
  name: "@frontity/mars-theme",
  roots: {
    /**
     *  In Frontity, any package can add React components to the site.
     *  We use roots for that, scoped to the `theme` namespace.
     */
    theme: Theme,
  },
  state: {
    /**
     * State is where the packages store their default settings and other
     * relevant state. It is scoped to the `theme` namespace.
     */
  
    auth:{
        header:'Authorization: Basic YWRtLXRhbjA5MzI6R21wUFBhN295TUdZQ1BrRnVN',
    },
    
    theme: {
      afterCSR: async ({ state, libraries, actions }) => {
        if (state.frontity.platform === 'client') {
          if (undefined === state.theme.token) {
            //actions.theme.fetchToken();
            state.auth.header="Authorization: Basic YWRtLXRhbjA5MzI6R21wUFBhN295TUdZQ1BrRnVN";
            actions.theme.FerramentasHandler();
          } else{
            //actions.theme.fetchToken();


          }
        }
      },
      //fetchToken,
   
      menu: [],
      isMobileMenuOpen: false,
      featured: {
        showOnList: false,
        showOnPost: false,
      },
    },
  },
  /**
   * Actions are functions that modify the state or deal with other parts of
   * Frontity like libraries.
   */
  actions: {
    theme: {
      init: ({ libraries }) => {
        // Add the handler to wp-source.
        libraries.source.handlers.push(signUpHandler);
        libraries.source.handlers.push(FerramentasHandler);
        

        
      },
      toggleMobileMenu: ({ state }) => {
        state.theme.isMobileMenuOpen = !state.theme.isMobileMenuOpen;
      },
      closeMobileMenu: ({ state }) => {
        state.theme.isMobileMenuOpen = false;
      },
      afterCSR: async ({ state, libraries, actions }) => {
     
        if (state.frontity.platform === 'client') {
          if (undefined === state.theme.token) {
            //actions.theme.fetchToken();
         
          }
        }
      },
     //fetchToken
    },
  },
  libraries: {
    html2react: {
      /**
       * Add a processor to `html2react` so it processes the `<img>` tags
       * inside the content HTML. You can add your own processors too
       */
      processors: [image, iframe],
    },
  },
};



export default marsTheme;
