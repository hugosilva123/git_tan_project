const settings = {
  "name": "frontity-example",
  "state": {
    "frontity": {
      "url": "http://tan.mybrontley.com/wp-json/",
      "title": "Tiago Almeida Nogueira",
      "description": "WordPress installation for Frontity development"
    }
  },

  "packages": [
    {
      "name": "@frontity/mars-theme",
      "state": {
        "theme": {
          "menu": [
            [
              "Home",
              "/",
            
            ],
            [
              "Sobre Mim",
              "/sobre-mim/"
            ],
            [
              "Parceiros",
              "/parceiros/"
            ],
            [
              "Agenda",
              "/agenda/"
            ],
            [
              "Dicas",
              "/dicas/"
            ],
            [
              "Ferramentas",
              "/ferramentas/"
            ],
            [
              " Contactos",
              "/teste/"
            ],
         
          ],

          "featured": {
            "showOnList": false,
            "showOnPost": false
          },
          "featuredMedia": {
            // Whether to show it on archive view
            "showOnArchive": true,
            // Whether to show it on post
            "showOnPost": true,
          }
        }
      }
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "api": "http://tan.mybrontley.com/wp-json/",
          "headers":  {
            'Content-Type': 'application/json',
            'Authorization':'Basic YWRtLXRhbjA5MzI6R21wUFBhN295TUdZQ1BrRnVN'
          },
        }
      }
    },
    "@frontity/tiny-router",
    "@frontity/html2react"
  ]
};

export default settings;
